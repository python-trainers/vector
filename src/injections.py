from trainerbase.codeinjection import AllocatingCodeInjection

from memory import stats_pointer, vector_s86


update_stats_pointer = AllocatingCodeInjection(
    vector_s86 + 0x1041E0,
    f"""
        mov [{stats_pointer}], ebx

        push dword [ebx + 0x10]
        mov [ebp - 0xDC], eax
    """,
    original_code_length=9,
)
