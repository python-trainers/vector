from typing import cast

from pymem.process import module_from_name
from pymem.ressources.structure import MODULEINFO
from trainerbase.memory import allocate_pointer, pm


vector_s86 = cast(MODULEINFO, module_from_name(pm.process_handle, "vector.s86")).lpBaseOfDll

stats_pointer = allocate_pointer()
