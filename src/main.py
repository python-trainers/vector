from trainerbase.main import run

from gui import run_menu
from injections import update_stats_pointer


def on_initialized():
    update_stats_pointer.inject()


if __name__ == "__main__":
    run(run_menu, on_initialized)
