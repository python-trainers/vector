from trainerbase.gameobject import GameUnsignedInt
from trainerbase.memory import Address

from memory import stats_pointer


stats_address = Address(stats_pointer)

stars = GameUnsignedInt(stats_address + [0xC])
money = GameUnsignedInt(stats_address + [0x10])
