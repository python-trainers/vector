from trainerbase.gui.helpers import add_components, simple_trainerbase_menu
from trainerbase.gui.misc import SeparatorUI, TextUI
from trainerbase.gui.objects import GameObjectUI
from trainerbase.gui.speedhack import SpeedHackUI

from objects import money, stars


@simple_trainerbase_menu("Vector", 640, 255)
def run_menu():
    add_components(
        TextUI("Pause and unpause the game. Or open market and close market."),
        GameObjectUI(stars, "Stars", default_setter_input_value=1000),
        GameObjectUI(money, "Money", default_setter_input_value=10_000_000),
        SeparatorUI(),
        SpeedHackUI(default_factor_input_value=0.35),
    )
